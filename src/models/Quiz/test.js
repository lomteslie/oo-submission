import ClassroomModel from '../Classroom'
import QuizModel from './index'
import QuizQuestionModel from '../QuizQuestion'
import StudentModel from '../Student'

let quiz = null

beforeEach(() => {
  quiz = new QuizModel({
    classroom: new ClassroomModel({}),
    questions: [
      new QuizQuestionModel({ correctChoice: 'A' }),
      new QuizQuestionModel({ correctChoice: 'B' }),
      new QuizQuestionModel({ correctChoice: 'C' })
    ]
  })
})

describe('Quiz model', () => {
  it('has correct default props', () => {
    expect(quiz.questions).toHaveLength(3)
    expect(quiz.grade).toBeNull()
    expect(quiz.submitted).toBe(false)
  })

  it('handles partial submission', () => {
    quiz.questions[0].answer('A')
    quiz.submit()
    expect(quiz.submitted).toBe(true)
    expect(quiz.complete).toBe(false)
    expect(quiz.grade).toBeNull()
  })

  it('handles complete submission', () => {
    quiz.questions[0].answer('A')
    quiz.questions[1].answer('B')
    quiz.questions[2].answer('A')
    quiz.submit()
    expect(quiz.submitted).toBe(true)
    expect(quiz.complete).toBe(true)
    expect(quiz.grade).toBe(67)
  })
})