function Quiz({
  classroom = {},
  questions = []
}) {
  /**
   * hasOne classroom
   *
   * @type {Classroom}
   */
  this.classroom = classroom

  /**
   * Has the quiz been completed?
   *
   * @type {Boolean}
   */
  this.complete = false

  /**
   * Grade out of 100. Null if no grade.
   *
   * @type {Number|null}
   */
  this.grade = null

  /**
   * hasMany students. Link the classroom students.
   *
   * @type {Array}
   */
  this.students = classroom.students || []

  /**
   * hasMany questions
   *
   * @type {Array}
   */
  this.questions = questions

  /**
   * Has the quiz been submitted?
   * It can be submitted without being completed.
   *
   * @type {Boolean}
   */
  this.submitted = false

  /**
   * Grade a set of questions. Set the local grade – rounded.
   *
   * @param  {Array} questions Questions to grade.
   * @return {void}
   * @private
   */
  this._gradeQuiz = questions => {
    const questionWeight = 100 / questions.length
    const grade = questions.reduce((total, question) => {
      return question.answeredCorrectly === true ? total + questionWeight : total
    }, 0)

    this.grade = Math.round(grade)
  }

  /**
   * Submit the current answers.
   *
   * @return {void}
   */
  this.submit = () => {
    this.submitted = true
    const answeredQuestions = this.questions
      .filter(question => question.answeredCorrectly !== null)

    if (answeredQuestions.length === this.questions.length) {
      this.complete = true
      this._gradeQuiz(answeredQuestions)
      this.students.forEach(x => x.addGrade(this.grade))
    }
  }
}

export default Quiz
