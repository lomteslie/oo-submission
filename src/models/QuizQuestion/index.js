function QuizQuestion({
  choices = [],
  correctChoice = null,
  quiz = null,
  title = null
}) {
  /**
   * Has this question been answered correctly?
   * Should be null if unanswered.
   *
   * @type {Boolean|null}
   */
  this.answeredCorrectly = null

  /**
   * List of possible answers.
   *
   * @type {Array}
   */
  this.choices = choices

  /**
   * String that matches the right choice, as defined in this.choices.
   *
   * @type {Array}
   */
  this.correctChoice = correctChoice

  /**
   * hasOne quiz
   *
   * @type {Quiz}
   */
  this.quiz = quiz

  /**
   * Title of the question.
   *
   * @type {String}
   */
  this.title = title

  /**
   * Answer a question. Mark it as correct or not.
   *
   * @param  {String} choice Chosen answer.
   * @return {void}
   */
  this.answer = choice => {
    this.answeredCorrectly = choice === this.correctChoice
  }

  return this
}

export default QuizQuestion
