import QuizQuestionModel from './index'
import Quiz from '../Quiz'

let quizQuestion = null

beforeEach(() => {
  quizQuestion = new QuizQuestionModel({
    title: 'Which U.S. state is the best?',
    choices: [
      'Massachusetts',
      'Tennessee',
      'Washington',
      'California'
    ],
    correctChoice: 'Massachusetts'
  })
})

describe('QuizQuestionModel model', () => {
  it('has correct default props', () => {
    expect(quizQuestion.title).toBe('Which U.S. state is the best?')
    expect(quizQuestion.choices).toHaveLength(4)
  })

  it('handles correctly answered question', () => {
    quizQuestion.answer('Massachusetts')

    expect(quizQuestion.answeredCorrectly).toBe(true)
  })

  it('handles incorrectly answered question', () => {
    quizQuestion.answer('Tennessee')

    expect(quizQuestion.answeredCorrectly).toBe(false)
  })
})