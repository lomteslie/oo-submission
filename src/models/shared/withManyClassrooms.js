/**
 * Model enhander for models that have classroom relationships.
 *
 * @param  {Function} Model The model to enhance
 * @return {Function}       The enhanced model.
 */
function withManyClassrooms(Model) {
  Model.prototype.classrooms = []

  /**
   * Add a classroom
   *
   * @param  {Number}  classroomId ID of the classroom being added.
   * @return {Teacher} this
   */
  Model.prototype.addClassroom = function(classroom) {
    this.classrooms.push(classroom)

    return this
  }

  /**
   * Remove a classroom
   *
   * @param  {Number}  classroomId ID of the classroom being removed.
   * @return {Teacher} this
   */
  Model.prototype.removeClassroom = function(classroom) {
    this.classrooms = this.classrooms.filter(cr => {
      return cr.id !== classroom.id
    })

    return this
  }

  return Model
}

export default withManyClassrooms
