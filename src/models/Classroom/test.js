import ClassroomModel from './index'
import TeacherModel from '../Teacher'
import StudentModel from '../Student'
import QuizModel from '../Quiz'

let classroom = null

beforeEach(() => {
  classroom = new ClassroomModel({
    teacher: new TeacherModel({}),
    students: [
      new StudentModel({}),
      new StudentModel({})
    ],
    quizzes: [
      new QuizModel({})
    ]
  })
})

describe('ClassroomModel model', () => {
  it('has correct default props', () => {
    expect(classroom.students).toHaveLength(2)
    expect(classroom.quizzes).toHaveLength(1)
  })
})