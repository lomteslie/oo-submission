function Classroom({
  teacher = null,
  students = [],
  quizzes = []
}) {
  /**
   * hasOne teacher
   *
   * @type {Teacher}
   */
  this.teacher = teacher

  /**
   * hasMany students
   *
   * @type {Array}
   */
  this.students = students

  /**
   * hasMany quizzes
   *
   * @type {Array}
   */
  this.quizzes = quizzes
}

export default Classroom
