import StudentModel from './index'

let student = null

beforeEach(() => {
  student = new StudentModel()
})

describe('Student model', () => {
  it('has correct default props', () => {
    expect(student.classrooms).toHaveLength(0)
  })

  it('adds a grade', () => {
    student.addGrade(55)
    expect(student.grades).toHaveLength(1)
    expect(student.grades[0]).toBe(55)
  })

  it('gets a total grade', () => {
    student.addGrade(55)
    student.addGrade(86)
    student.addGrade(73)
    const total = student.getGradeAverage()
    expect(total).toBe(71)
  })

  it('returns null when there are no grades', () => {
    const total = student.getGradeAverage()
    expect(total).toBeNull()
  })
})