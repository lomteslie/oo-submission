import withManyClassrooms from '../shared/withManyClassrooms'

function Student() {
  /**
   * hasMany classrooms
   *
   * @type {Array}
   */
  this.classrooms = []

  /**
   * hasMany grades
   *
   * @type {Array}
   */
  this.grades = []

  /**
   * Add a quiz grade for reference later.
   *
   * @param  {Number} grade Quiz grade.
   * @return {void}
   */
  this.addGrade = grade => {
    this.grades.push(grade)
  }

  /**
   * Get the student’s grade average from the stored grades.
   *
   * @return {Number|null} Null if there are no grades.
   */
  this.getGradeAverage = () => {
    if (!this.grades.length) {
      return null
    }

    const total = this.grades.reduce((total, grade) => {
      return total + grade
    }, 0)

    return Math.round(total / this.grades.length)
  }
}

export default withManyClassrooms(Student)
