import Quiz from '../Quiz'
import withManyClassrooms from '../shared/withManyClassrooms'

function Teacher() {
  /**
   * hasMany classrooms
   *
   * @type {Array}
   */
  this.classrooms = []

  /**
   * Create a quiz and assign it to the specified classroom.
   *
   * @param  {Object} options.classroom Classroom to give the quiz to.
   * @param  {Array}  options.questions List of questions for the quiz.
   * @return {void}
   */
  this.createQuiz = ({ classroom, questions }) => {
    const match = this.classrooms.find(x => x.id === classroom.id)

    if (match) {
      match.quizzes.push(new Quiz({ classroom, questions }))
    }
  }
}

export default withManyClassrooms(Teacher)
