import Classroom from '../Classroom'
import QuizQuestion from '../QuizQuestion'
import TeacherModel from './index'

describe('Teacher model', () => {
  let teacher = null

  beforeEach(() => {
    teacher = new TeacherModel()
  })

  it('has correct default props', () => {
    expect(teacher.classrooms).toHaveLength(0)
  })

  it('adds a new classroom', () => {
    teacher.addClassroom(1)
    expect(teacher.classrooms).toHaveLength(1)
  })

  it('removes a classroom', () => {
    teacher
      .addClassroom(1)
      .removeClassroom(1)
    expect(teacher.classrooms).toHaveLength(0)
  })

  it('does nothing when removed classroom doesn’t exist', () => {
    teacher.removeClassroom(5)
    expect(teacher.classrooms).toHaveLength(0)
  })
})