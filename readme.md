# Object Oriented Exercise Submission
> By Tom Leslie

## Hello!
For this submission I decided to create a set of **Model** directories, with their included index and test files. I had a lot of fun doing this, especially in such a short amount of time. Given more time I would optimize things further.

### React Exercise
The react exercise is located here at `react-submission.js`.

### Run Tests
`yarn test`