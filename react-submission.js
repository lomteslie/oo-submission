/**
* This React class is intended to query an endpoint that will return an alphanumeric string, after clicking a button.
* This component is passed a prop "apiQueryDelay", which delays the endpoint request by N milliseconds. There is a
* second button to disable this functionality and have the endpoint request run immediately after button click.
* This data is then to be displayed inside a simple container.
* The "queryAPI" XHR handler will return the endpoint response in the form of a Promise (such as axios, fetch).
* The response object will look like the following: {data: "A0B3HCJ"}
* The containing element ref isn't used, but should remain within the class.
* Please identify, correct and comment on any errors or bad practices you see in the React component class below.
* Additionally, please feel free to change the code style as you see fit.
* Please note - React version for this exercise is 15.5.4
*/

// @fix - alphabetized dependencies.
// @fix - Component needed to be destructured here.
// @fix - add propTypes
import propTypes from 'prop-types'
import queryAPI from 'queryAPI';
import React, { Component } from 'react';


class ShowResultsFromAPI extends Component() {
  // @fix - added missing props argument
  constructor(props) {
    super(props);

    this.container = null;

    // @fix - Adding initial state
    this.state = {
      apiQueryDelay: 0,
      data: null,
      error: false
    }
  }

  // @fix - adding componentDidMount to set the state with props
  componentDidMount() {
    const { apiQueryDelay } = this.props
    this.setState({ apiQueryDelay })
  }

  onDisableDelay() {
    // @fix - shouldn’t set props ever. changing to setState
    this.setState({ apiQueryDelay: 0 })
  }

  click() {
    // @fix - Cleaning this up to look a little nicer. Also fetching data without delay
    const { apiQueryDelay } = this.state

    if (apiQueryDelay) {
      setTimeout(this.fetchData.bind(this), apiQueryDelay);
    } else {
      this.fetchData()
    }
  }

  fetchData() {
    queryAPI()
      .then(function(response) {
        if (response.data) {
          this.setState({
            data: response.data,
            error: false
          });
        }
      })
      // @fix - Add a catch to handle errors
      .catch(function(error) {
        this.setState({ error: true });
      });
  }

  render() {
    // @fix - add parentheses. cleaning up
    return (
      // @fix - change ref to function call
      <div class="content-container" ref={el => this.container = el}>
        {!!error && <p>Sorry - there was an error with your request.</p>}
        {!error && <p>data</p>}
        <Button onClick={this.onDisableDelay.bind(this)}>Disable request delay</Button>
        <Button onClick={this.click.bind(this)}>Request data from endpoint</Button>
      </div>
    )
  }
}

ShowResultsFromAPI.displayName = {
  name: "ShowResultsFromAPI"
};
ShowResultsFromAPI.defaultProps = {
  apiQueryDelay: 0
};
// @fix - Use separate propTypes library
ShowResultsFromAPI.propTypes = {
  apiQueryDelay: propTypes.number
};

// @fix - insert default
export default ContentContainer;
